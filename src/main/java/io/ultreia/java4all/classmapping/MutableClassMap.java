package io.ultreia.java4all.classmapping;

/*-
 * #%L
 * Class Mapping tools
 * %%
 * Copyright (C) 2017 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * A special map to store string values using the {@link #key0(Class)} method to produce keys.
 * <p>
 * Created by tchemit on 01/09/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class MutableClassMap<V> {

    private final ImmutableClassMap.KeyFunction keyFunction;
    private final Map<String, V> data;

    public static <V> MutableClassMap<V> create(ImmutableClassMap.KeyFunction keyFunction) {
        return new MutableClassMap<>(keyFunction);
    }

    protected MutableClassMap(ImmutableClassMap.KeyFunction keyFunction, MutableClassMap<V> dtoMap) {
        this.keyFunction = keyFunction;
        this.data = dtoMap.data;
    }

    private MutableClassMap(ImmutableClassMap.KeyFunction keyFunction) {
        this.keyFunction = keyFunction;
        this.data = new TreeMap<>();
    }

    public int size() {
        return data.size();
    }

    public boolean isEmpty() {
        return data.isEmpty();
    }

    public V get(Class key) {
        return data.get(key0(key));
    }

    public V get(String key) {
        return data.get(key0(key));
    }

    public Set<String> keySet() {
        return data.keySet();
    }

    public Collection<V> values() {
        return data.values();
    }

    public void put(Class type, V value) {
        data.put(key0(type), value);
    }

    public void clear() {
        data.clear();
    }

    private String key0(Class type) {
        return keyFunction.key(type);
    }

    private String key0(String type) {
        return keyFunction.key(type);
    }

}
