package io.ultreia.java4all.classmapping;

/*-
 * #%L
 * Class Mapping tools
 * %%
 * Copyright (C) 2017 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * Created by tchemit on 20/09/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public abstract class ImmutableClassMap<V> {

    protected final KeyFunction keyFunction;
    protected final Map<String, V> data;
    protected final Map<String, Class<?>> types;

    protected static abstract class BuilderSupport<V, M extends ImmutableClassMap<V>> {

        protected final KeyFunction keyFunction;
        protected final TreeMap<String, V> data;
        protected final TreeMap<String, Class<?>> types;

        protected BuilderSupport(KeyFunction keyFunction) {
            this.keyFunction = keyFunction;
            this.data = new TreeMap<>();
            this.types = new TreeMap<>();
        }

        protected abstract M build(KeyFunction keyFunction, Map<String, V> data, Map<String, Class<?>> types);

        public BuilderSupport<V, M> put(Class<?> type, V value) {
            String key = keyFunction.key(type);
            types.put(key, type);
            data.put(key, value);
            return this;
        }

        public M build() {
            return build(this.keyFunction, data, types);
        }
    }

    protected ImmutableClassMap(ImmutableClassMap<V> dtoMap) {
        this.keyFunction = dtoMap.keyFunction;
        this.data = dtoMap.data;
        this.types = dtoMap.types;
    }

    protected ImmutableClassMap(KeyFunction keyFunction, Map<String, V> data, Map<String, Class<?>> types) {
        this.keyFunction = keyFunction;
        this.data = Collections.unmodifiableMap(data);
        this.types = Collections.unmodifiableMap(types);
    }

    public int size() {
        return data.size();
    }

    public boolean isEmpty() {
        return data.isEmpty();
    }

    public V get(Class<?> key) {
        return data.get(key0(key));
    }

    public V get(String key) {
        return data.get(key0(key));
    }

    public Set<String> keySet() {
        return data.keySet();
    }

    public Collection<V> values() {
        return data.values();
    }

    private String key0(Class<?> type) {
        return keyFunction.key(type);
    }

    private String key0(String type) {
        return keyFunction.key(type);
    }

    public interface KeyFunction {
        String key(Class<?> type);

        String key(String type);
    }
}
