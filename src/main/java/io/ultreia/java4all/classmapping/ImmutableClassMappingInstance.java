package io.ultreia.java4all.classmapping;

/*-
 * #%L
 * Class Mapping tools
 * %%
 * Copyright (C) 2017 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.Supplier;

/**
 * Created on 08/02/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0.4
 */
public class ImmutableClassMappingInstance<K, V> {

    private final Map<Class<? extends K>, Supplier<? extends V>> mapping;

    public static class Builder<K, V> {

        private final Map<Class<? extends K>, Supplier<? extends V>> mappingBuilder = new LinkedHashMap<>();

        public Builder<K, V> put(Class<? extends K> k, Supplier<? extends V> v) {
            mappingBuilder.put(k, v);
            return this;
        }

        public ImmutableClassMappingInstance<K, V> build() {
            return new ImmutableClassMappingInstance<>(getMappingBuilder());
        }

        public Map<Class<? extends K>, Supplier<? extends V>> getMappingBuilder() {
            return mappingBuilder;
        }
    }

    public static <K, V> Builder<K, V> builder() {
        return new Builder<>();
    }

    protected ImmutableClassMappingInstance(Map<Class<? extends K>, Supplier<? extends V>> mapping) {
        this.mapping = Collections.unmodifiableMap(mapping);
    }

    public boolean containsKey(Class<K> key) {
        return mapping.containsKey(key);
    }

    public <VV extends V> VV create(Class<?> key) {
        @SuppressWarnings("unchecked") Supplier<VV> supplier = (Supplier<VV>) mapping.get(key);
        return supplier == null ? null : supplier.get();
    }

    public int size() {
        return mapping.size();
    }

    protected Map<Class<? extends K>, Supplier<? extends V>> getMapping() {
        return mapping;
    }

}

