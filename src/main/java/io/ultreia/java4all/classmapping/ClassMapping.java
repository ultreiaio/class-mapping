package io.ultreia.java4all.classmapping;

/*-
 * #%L
 * Class Mapping tools
 * %%
 * Copyright (C) 2017 - 2022 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Created by tchemit on 16/07/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ClassMapping<K, V> {

    private final Map<Class, Class> mapping = new LinkedHashMap<>();

    private final String keyPackage;
    private final String implementationType;
    private final String classSuffix;

    public ClassMapping(String keyPackage, String implementationType, String classSuffix) {
        this.keyPackage = keyPackage;
        this.implementationType = implementationType;
        this.classSuffix = classSuffix;
    }

    protected ClassMapping(Package keyPackage, Package implementationType, String classSuffix) {
        this(keyPackage.getName(), implementationType.getName(), classSuffix);
    }

    @SuppressWarnings("unchecked")
    public <VV extends V> Class<VV> get(Class<? extends K> type) {
        Objects.requireNonNull(type);
        return mapping.computeIfAbsent(type, t -> {
            try {
                return getClass0(type);
            } catch (ClassNotFoundException e) {
                throw new IllegalStateException("Can't find target class for " + type.getName());
            }
        });
    }

    @SuppressWarnings("WeakerAccess")
    protected Class getClass0(Class<?> type) throws ClassNotFoundException {
        String packageSuffix = type.getPackage().getName().substring(keyPackage.length());
        String className = implementationType + packageSuffix + "." + type.getSimpleName() + classSuffix;
        return Class.forName(className);
    }


    protected int size() {
        return mapping.size();
    }
}
