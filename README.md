# Class mapping

This project offers some class mapping implementations.

[![Maven Central status](https://img.shields.io/maven-central/v/io.ultreia.java4all/class-mapping.svg)](https://search.maven.org/#search%7Cga%7C1%7Cg%3A%22io.ultreia.java4all%22%20AND%20a%3A%22class-mapping%22)
![Build Status](https://gitlab.com/ultreiaio/class-mapping/badges/develop/pipeline.svg)
[![The Less GNU General Public License, Version 3.0](https://img.shields.io/badge/license-LGPL3-green.svg)](http://www.gnu.org/licenses/lgpl-3.0.txt)

# Resources

* [Changelog and downloads](https://gitlab.com/ultreiaio/class-mapping/blob/develop/CHANGELOG.md)
* [Documentation](https://ultreiaio.gitlab.io/class-mapping)

# Community

* [Contact](mailto:dev+class-mapping@tchemit.fr)
