# Class Mapping changelog

* Author [Tony Chemit](mailto:dev@tchemit.fr)
* Last generated at 2018-07-30 11:43.

## Version [1.0.3](https://gitlab.com/ultreiaio/class-mapping/milestones/4)

**Closed at 2018-07-30.**

### Issues

* [[enhancement 1]](https://gitlab.com/ultreiaio/class-mapping/issues/1) **Migrates to Log4J2** (Thanks to Tony
  CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.2](https://gitlab.com/ultreiaio/class-mapping/milestones/3)

**Closed at *In progress*.**

### Issues

No issue.

## Version [1.0.1](https://gitlab.com/ultreiaio/class-mapping/milestones/2)

**Closed at *In progress*.**

### Issues

No issue.

## Version [1.0.0](https://gitlab.com/ultreiaio/class-mapping/milestones/1)

**Closed at *In progress*.**

### Issues

No issue.

