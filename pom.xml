<?xml version="1.0" encoding="UTF-8"?>
<!--
  #%L
  Class Mapping tools
  %%
  Copyright (C) 2017 - 2022 Ultreia.io
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  You should have received a copy of the GNU General Lesser Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-3.0.html>.
  #L%
  -->
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>
  <parent>
    <groupId>io.ultreia.maven</groupId>
    <artifactId>pom</artifactId>
    <version>2022.16</version>
  </parent>
  <groupId>io.ultreia.java4all</groupId>
  <artifactId>class-mapping</artifactId>
  <version>1.0.5-SNAPSHOT</version>
  <name>Class Mapping tools</name>
  <description>Class Mapping tools</description>
  <url>https://ultreiaio.gitlab.io/class-mapping</url>
  <inceptionYear>2017</inceptionYear>
  <scm>
    <connection>scm:git:git@gitlab.com:${projectPath}.git</connection>
    <developerConnection>scm:git:ssh://git@gitlab.com:${projectPath}.git</developerConnection>
    <url>https://gitlab.com/${projectPath}</url>
  </scm>
  <distributionManagement>
    <site>
      <id>gitlab.com</id>
      <url>scm:git:https://git@gitlab.com/${projectPath}.git</url>
    </site>
  </distributionManagement>
  <properties>
    <java.version>11</java.version>
    <organizationId>ultreiaio</organizationId>
    <projectId>class-mapping</projectId>
    <locales>en</locales>
    <maven.javadoc.skip>false</maven.javadoc.skip>
    <maven.source.skip>false</maven.source.skip>
    <maven.deploy.skip>false</maven.deploy.skip>
    <gitlab.changesTitle>Class Mapping changelog</gitlab.changesTitle>
  </properties>
  <profiles>
    <profile>
      <id>reporting</id>
      <activation>
        <property>
          <name>performRelease</name>
          <value>true</value>
        </property>
      </activation>
      <reporting>
        <plugins>
          <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-project-info-reports-plugin</artifactId>
            <version>${plugin.version.projectInfoReports}</version>
          </plugin>
          <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-changes-plugin</artifactId>
            <version>${plugin.version.changes}</version>
            <inherited>false</inherited>
            <configuration>
              <issueLinkTemplatePerSystem>
                <gitlab>https://gitlab.com/${projectPath}/issues/%ISSUE%</gitlab>
              </issueLinkTemplatePerSystem>
              <xmlPath>${project.build.directory}/generated-site/changes.xml</xmlPath>
            </configuration>
            <reportSets>
              <reportSet>
                <reports>
                  <report>changes-report</report>
                </reports>
              </reportSet>
            </reportSets>
          </plugin>
          <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-javadoc-plugin</artifactId>
            <version>${plugin.version.javadoc}</version>
            <reportSets>
              <reportSet>
                <reports>
                  <report>javadoc-no-fork</report>
                  <report>test-javadoc-no-fork</report>
                </reports>
              </reportSet>
            </reportSets>
          </plugin>
          <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-changelog-plugin</artifactId>
            <version>${plugin.version.changelog}</version>
          </plugin>
          <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-jxr-plugin</artifactId>
            <version>${plugin.version.jrx}</version>
            <reportSets>
              <reportSet>
                <reports>
                  <report>jxr-no-fork</report>
                  <report>test-jxr-no-fork</report>
                </reports>
              </reportSet>
            </reportSets>
          </plugin>
          <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-surefire-report-plugin</artifactId>
            <version>${plugin.version.surefire}</version>
          </plugin>
          <plugin>
            <groupId>org.codehaus.mojo</groupId>
            <artifactId>license-maven-plugin</artifactId>
            <version>${plugin.version.license}</version>
            <reportSets>
              <reportSet>
                <reports>
                  <report>third-party-report</report>
                </reports>
              </reportSet>
            </reportSets>
          </plugin>
          <plugin>
            <groupId>org.codehaus.mojo</groupId>
            <artifactId>versions-maven-plugin</artifactId>
            <version>${plugin.version.versions}</version>
            <reportSets>
              <reportSet>
                <reports>
                  <report>dependency-updates-report</report>
                  <report>plugin-updates-report</report>
                </reports>
              </reportSet>
            </reportSets>
          </plugin>
        </plugins>
      </reporting>
    </profile>
  </profiles>
</project>
